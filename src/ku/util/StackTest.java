package ku.util;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
/**
 * JUnit test for the Stack case for two differents stack
 * @author Thanawit Gerdprasert.
 *
 */
public class StackTest {
	private Stack stack1;
	private Stack stack2;
	/** "Before" method is run before each test. */
	@Before
	public void setUp( ) {

		StackFactory.setStackType(0);
		stack1 = StackFactory.makeStack( 1 );
		StackFactory.setStackType(1);
		stack2 = StackFactory.makeStack(3);
	}
	
	//my test

	@Test
	public void testStackTypeOne ()
	{
		stack1.push("obj1");
		stack1.push("obj2");
		stack1.push("obj3");
		assertEquals( stack1.pop() , "obj3");
		assertSame( stack1.capacity() , 1);
		
	}
	
	@Test ( expected = java.lang.IllegalStateException.class )
	public void testStateException()
	{
		stack1.push("obj1");
		stack1.push("obj2");
		stack1.push("obj3");
	}
	
	@Test ( expected = java.lang.IllegalArgumentException.class )
	public void testNullPush()
	{
		stack2.push(null);
	}
	
	
	
	
	//end my test
	
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack1.isEmpty() );
		assertFalse( stack1.isFull() );
		assertEquals( 0, stack1.size() );
	}
	/** pop() should throw an exception if stack is empty */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack1.isEmpty() );
		stack1.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}
}
